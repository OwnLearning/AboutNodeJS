var http=require('http');
//载入http模块
const hostname='127.0.0.1';
//设置ip
const port=3000;
//设置端口
const server=http.createServer((req,res) =>{
	res.statusCode=200;
	res.setHeader('Content-Type','text/plain');
	res.end('Hello World\n');
});
//创建服务器
server.listen(port,hostname,()=>{
	console.log('Server running at http://'+hostname+':'+port+'/');
});
//设置监听