var events=require('events');
console.log("it is beginning");
var eventEmitter=new events.EventEmitter();
//创建eventEmitter对象
var connectHandler=function connected(){
	console.log("connected ");
	eventEmitter.emit('data_received');
	//触发事件
}
eventEmitter.on('connection',connectHandler);
//绑定connection事件处理程序
eventEmitter.on('data_received',function(){
	console.log('received data');
});
//使用匿名函数接收数据
eventEmitter.emit('connection');
console.log("it is over");